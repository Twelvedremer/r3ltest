//
//  ViewController.swift
//  R3LTest
//
//  Created by Momentum Lab 1 on 1/25/18.
//  Copyright © 2018 MomentumLab. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {

  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var countLabel: UIBarButtonItem!
  @IBOutlet weak var totalLabel: UIBarButtonItem!
  
  var viewModel = ShoppingViewModel()
  private let bag = DisposeBag()
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.viewModel.openStore()
    
    self.viewModel.onSale.asObservable().subscribe({ [weak self] _ in
      DispatchQueue.main.async { [weak self] in
        self?.tableView.reloadData()
      }
    }).disposed(by: bag)
    
    self.viewModel
        .onCart.asObservable()
        .map { products -> Int in
            return products.reduce(0, { (total, product) -> Int in
                return total + product.cart
            })}
      .map { total -> String in
        return "\(total)"}
      .bind(to: countLabel.rx.title)
      .disposed(by: bag)
    
    self.viewModel
      .onCart.asObservable()
      .map { products -> Float in
        return products.reduce(0.0, { (total, product) -> Float in
          return total + (product.price * Float(product.cart))
        })}
      .map { total -> String in
        return "\(total)$"}
      .bind(to: totalLabel.rx.title)
      .disposed(by: bag)

  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if let view = segue.destination as? CartViewController{
      view.viewModel = self.viewModel
    }
  }
  
}

extension ViewController: UITableViewDataSource{
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.onSale.value.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "StoreCell", for: indexPath) as! StoreTableViewCell
    cell.load(item: viewModel.onSale.value[indexPath.row])
    cell.selectionStyle = .none
    cell.replay.subscribe(onNext: { product in
      self.viewModel.update(Product: product)
    }).disposed(by: bag)
    return cell
  }
}




