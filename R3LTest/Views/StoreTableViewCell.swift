//
//  StoreTableViewCell.swift
//  R3LTest
//
//  Created by Momentum Lab 1 on 1/25/18.
//  Copyright © 2018 MomentumLab. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class StoreTableViewCell: UITableViewCell {

  @IBOutlet weak var product: UILabel!
  @IBOutlet weak var price: UILabel!
  @IBOutlet weak var stock: UILabel!
  
  var Itemproduct: Product!
  var replay = PublishSubject<Product>()

  override func awakeFromNib() {
        super.awakeFromNib()
    }

  override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
  
  func load(item: Product){
    self.Itemproduct = item
    self.product.text = Itemproduct.name
    self.stock.text = "stock: \(Itemproduct.available)"
    guard let priceInfo = Itemproduct.price else {
      self.price.text = "0$"
      return
    }
    self.price.text = "\(priceInfo)$"
    
  }

  @IBAction func buyProduct(_ sender: Any) {
    Itemproduct.cart += 1
    replay.onNext(Itemproduct)
  }
  

}
