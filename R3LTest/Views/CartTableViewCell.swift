//
//  CartTableViewCell.swift
//  R3LTest
//
//  Created by Momentum Lab 1 on 1/25/18.
//  Copyright © 2018 MomentumLab. All rights reserved.
//

import UIKit
import RxSwift

class CartTableViewCell: UITableViewCell {

  @IBOutlet weak var product: UILabel!
  @IBOutlet weak var purchased: UILabel!
  var Itemproduct: Product!
  var replay = PublishSubject<Product>()
  
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
  
  func load(item: Product){
    self.Itemproduct = item
    self.product.text = Itemproduct.name
    self.purchased.text = "stock: \(Itemproduct.cart)"
  }
  
  @IBAction func returnProduct(_ sender: Any) {
    Itemproduct.cart -= 1
    replay.onNext(Itemproduct)
  }

}
