//
//  CartViewController.swift
//  R3LTest
//
//  Created by Momentum Lab 1 on 1/25/18.
//  Copyright © 2018 MomentumLab. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CartViewController: UIViewController {

  @IBOutlet weak var tableView: UITableView!
  var viewModel: ShoppingViewModel!
  private let bag = DisposeBag()
  
    override func viewDidLoad() {
        super.viewDidLoad()

      self.viewModel.onSale.asObservable().subscribe({ [weak self] _ in
        DispatchQueue.main.async { [weak self] in
          self?.tableView.reloadData()
        }
      }).disposed(by: bag)
    }
  
  @IBAction func dismissModal(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
  
}

extension CartViewController: UITableViewDataSource{
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.onCart.value.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as! CartTableViewCell
    cell.load(item: viewModel.onCart.value[indexPath.row])
    cell.selectionStyle = .none
    cell.replay.subscribe(onNext: { product in
      self.viewModel.update(Product: product)
    }).disposed(by: bag)
    return cell
  }
}

