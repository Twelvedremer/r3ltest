//
//  Products.swift
//  R3LTest
//
//  Created by Momentum Lab 1 on 1/25/18.
//  Copyright © 2018 MomentumLab. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Product{
  var name: String!
  var stock: Int!
  var price: Float!
  var cart: Int = 0
  
  var available: Int{
    let items = stock - cart
    return items > 0 ? items : 0
  }

  init?(json: JSON) {
    
    guard let name = json["name"].string, let stock = json["stock"].int, let price = json["price"].float else {
      return nil
    }
    
    self.name = name
    self.stock = stock
    self.price = price
  }
  
}
