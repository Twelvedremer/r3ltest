//
//  ShoppingViewModel.swift
//  R3LTest
//
//  Created by Momentum Lab 1 on 1/25/18.
//  Copyright © 2018 MomentumLab. All rights reserved.
//

import Foundation
import SwiftyJSON
import RxSwift

class ShoppingViewModel{
  
 
  var onSale = Variable<[Product]>([])
  var onCart = Variable<[Product]>([])
  
  private var products = Variable<[Product]>([])
  private let bag = DisposeBag()
  
  init() {
    products
      .asObservable()
      .map({ (products) -> [Product] in
        return products.filter({ product -> Bool in
          return product.available > 0
        })})
      .bind(to: onSale)
      .disposed(by: bag)
    
    
    products
      .asObservable()
      .map({ (products) -> [Product] in
        return products.filter({ product -> Bool in
          return product.cart > 0
        })})
      .bind(to: onCart)
      .disposed(by: bag)
  }
  
  public func openStore(){
    products.value = initProduct()
  }
  
  public func update(Product item: Product){
    if let index =  self.products.value.index(where: { product -> Bool in
      return product.name == item.name && product.price == item.price && product.stock == item.stock
    }){
      self.products.value[index] = item
    }
    
  }
  
  private func initProduct() -> [Product]{
    guard let path = Bundle.main.path(forResource: "product", ofType: "json"),
      let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
        return []
    }
    do{
      guard let jsonArray = try JSON(data: data).array else {
        return []
      }
      
      return jsonArray.flatMap({ json -> Product? in
            return Product(json: json)
      })
    } catch {
      print("The file could not be read")
    }
    return []
  }
}
